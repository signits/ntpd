ntpd
=========

The ntpd module installs the package ntp, and enables the service according to the parameters provided

Requirements
------------

None

Role Variables
--------------

enable_ntpd: true - [controls if the module will run at all]

service_state: "started" - [whether service runs]

service_enable: "yes" - [whether service is enabled]

daemon_extra_opts: "-p /var/run/ntpd.pid" - [extra options for ntpd]

servers: [ "10.250.100.6", "10.250.100.7" ] - [ntpd servers list]

restrict_params: ["default kod nomodify notrap nopeer noquery", "-6 default kod nomodify notrap nopeer noquery", "127.0.0.1", "-6 ::1"] - [ntpd restrict parameters]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
